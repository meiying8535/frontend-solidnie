    // VIEW:
    // function makeRow(data){
    //     return $('<tr data-id="'+ data.i +'">\
    //         <td>'+ data.i +'</td>\
    //         <td>'+ data.title +'</td>\
    //         <td>'+ data.author +'</td>\
    //         <td class="del"> &times; </td>\
    //     </tr>')
    // }
    // function clearRows(){
    //     playlistRows.empty()
    // }
    // function renderRows(data){
    //     clearRows()
    //     data.forEach(function(item){
    //         playlistRows.append(makeRow(item))
    //     })
    //     renderCounter(data.length)
    // }
    // function renderCounter(count){
    //     playlistTable.find('.counter').html(count)
    // }

    function ItemsView(elem,options){
        this.el = elem;
        this.makeItem = options.makeItem;
        this.counter = options.counter;
    }
    ItemsView.prototype = {
        makeItem: function(){},
        clearItems: function(){
            this.el.empty()
        },
        renderItems: function(data){
            this.clearItems();
            data.forEach(function(item){
                this.el.append(this.makeItem(item))
            }.bind(this))
            this.counter.html(data.length)
        },
    }

    function ItemsModel(items, views){
        this.items = items;
        this.views = views;
    }
    ItemsModel.prototype = {
        notifyView: function(){
            this.views.forEach(function(view){
                view.renderItems(this.items)
            }.bind(this))
        },
        addItem: function(item){
            item.i = this.items.length + 1;
            this.items.push(item)
            this.notifyView()
        },
        getItem(id){
            return this.items.filter(function(item){
                return item.i == id
            }).pop()
        },
        removeItem:function(item){
            var index = this.items.indexOf(item)
            this.items.splice(index,1)
            this.notifyView()
        },
        selectItem: function(item){
            this.selected = item
            this.notifyView()
        }
    }


    // MODEL:
    var model = {
        // playlist:{
        //     items: [],
        //     notifyView: function(){
        //         playlistView.renderItems(this.items)
        //         playlistView2.renderItems(this.items)
        //     },
        //     addItem: function(item){
        //         item.i = this.items.length + 1;
        //         this.items.push(item)
        //         this.notifyView()
        //     },
        //     getItem(id){
        //         return this.items.filter(function(item){
        //             return item.i == id
        //         }).pop()
        //     },
        //     removeItem:function(item){
        //         var index = this.items.indexOf(item)
        //         this.items.splice(index,1)
        //         this.notifyView()
        //     },
        //     selectItem: function(item){
        //         this.selected = item
        //         this.notifyView()
        //     }
        // }
    }
//    model.playlist.notifyView()

    // CONTROLLER:
    function ItemsController(form, list, model){
        this.form = form; this.list = list;
        this.fields = this.form.get(0).elements;
        this.model = model;

        this.list.on('click','.del', function(e){
            var id = $(this).closest('tr').data('id');
            var item = this.model.getItem(id)
            this.model.removeItem(item)
        }.bind(this));

        this.form.on('submit', this.onSubmit.bind(this) )
    }
    ItemsController.prototype = {
        onSubmit: function(event){
            var item = {
                title: this.fields.title.value,
                author: this.fields.author.value 
            };
            this.model.addItem(item)
            event.preventDefault();
        }
    }
    